<?php
/**
 * Created by PhpStorm.
 * User: Cédric Gournay
 */

namespace ExportBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class ExportController extends Controller
{
    /**
     * @return Response
     */
    public function exportAction()
    {
        $export = $this->get('export.exporttocsv')->exportService();

        return $export;
    }
}