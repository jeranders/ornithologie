<?php
/**
 * Created by PhpStorm.
 * User: Cédric Gournay
 */

namespace ExportBundle\Service;

use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Response;

class ExportToCsv
{
    /**
     * @var EntityManager
     */
    private $doctrine;

    /**
     * Export constructor.
     * @param EntityManager $doctrine
     */
    public function __construct(EntityManager $doctrine)
    {
        $this->doctrine = $doctrine;
    }

    public function exportService()
    {
        $answers = $this->doctrine->getRepository('ObservationBundle:Observation')->findAll();
        $handle = fopen('php://memory', 'r+');

        $header = array();

        fputcsv($handle, array(
            'Numero de saisie',
            'Nom Valide',
            'Nom vernaculaire',
            'Famille',
            'Nombre',
            'Date',
            'Latitude',
            'Longitude',
            'Saison',
            'Precipitation',
            'Meteo',
            'Periode',
            'Environnement',
            'Comportement',
            'Sensibilite',
            'Commentaire'
        ),';'
        );

        foreach ($answers as $answer)
        {
            fputcsv($handle, array(
                $answer->getId(),
                $answer->getSpecies()->getNomValide(),
                $answer->getSpecies()->getNomVern(),
                $answer->getSpecies()->getFamille(),
                $answer->getNombre(),
                $answer->getDate()->format('Y-m-d H:i:s'),
                $answer->getLatitude(),
                $answer->getLongitude(),
                $answer->getSaison(),
                $answer->getPrecipitation(),
                $answer->getMeteo(),
                $answer->getPeriode(),
                $answer->getenvironnement(),
                $answer->getComportement(),
                $answer->getSensibilite(),
                $answer->getCommentaire()
            ),';'
            );
        }

        rewind($handle);
        $content = stream_get_contents($handle);
        fclose($handle);

        $export = new Response($content, 200, array(
            'Content-Type' => 'application/force-download',
            'Content-Disposition' => 'attachment; filename="export_nao.csv"'
        ));

        return $export;
    }
}