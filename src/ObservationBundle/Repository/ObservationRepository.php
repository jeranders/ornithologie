<?php

namespace ObservationBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;
use Doctrine\ORM\Tools\Pagination\Paginator;

class ObservationRepository extends EntityRepository
{
    /**
     * @return mixed
     */
    public function countAllObservation()
    {
        return $this->createQueryBuilder('o')
            ->select('COUNT(o.id)')
            ->getQuery()
            ->getSingleScalarResult();
    }

    /**
     * @return mixed
     */
    public function afficheGPS()
    {
        return $this->createQueryBuilder('o')
            ->select('COUNT(o.id)')
            ->where('o.gpsAffiche = 1')
            ->andWhere('o.valide = 1')
            ->getQuery()
            ->getSingleScalarResult();
    }
    /**
     * @return mixed
     */
    public function countValideObservation()
    {
        return $this->createQueryBuilder('o')
            ->select('COUNT(o.valide)')
            ->where('o.valide = 1')
            ->getQuery()
            ->getSingleScalarResult();
    }

    /**
     * @param $id
     * @return array
     */
    public function infoObservation($id)
    {
        return $this->createQueryBuilder('o')
            ->leftJoin('o.species', 't')
            ->addSelect('t')
            ->where('o.id = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getResult();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function countObservationEspece($id)
    {
        return $this->createQueryBuilder('o')
            ->select('COUNT(o)')
            ->where('o.species = :id')
            ->andWhere('o.valide = 1')
            ->setParameter('id', $id)
            ->getQuery()
            ->getSingleScalarResult();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function listMapObservation($id)
    {
        return $this->createQueryBuilder('o')
            ->select('o')
            ->where('o.species = :id')
            ->andWhere('o.valide = 1')
            ->andWhere('o.gpsAffiche = 1')
            ->setParameter('id', $id)
            ->getQuery()
            ->getResult();
    }

    /**
     * @return array
     */
    public function lastObs()
    {
        return $this->createQueryBuilder('o')
            ->select('o')
            ->andWhere('o.valide = 1')
            ->orderBy('o.id', 'DESC')
            ->setMaxResults(8)
            ->getQuery()
            ->getResult();
    }

    /**
     * @param $id
     * @return array
     */
    public function lastArraySaisie($id)
    {
        return $this->createQueryBuilder('o')
            ->select('o')
            ->where('o.species = :id')
            ->andWhere('o.valide = 1')
            ->setParameter('id', $id)
            ->getQuery()
            ->getResult();
    }

    /**
     * @param $id
     * @return array
     */
    public function oneImage($id)
    {
        return $this->createQueryBuilder('o')
            ->select('o')
            ->leftJoin('o.image', 'i')
            ->where('o.species = :id')
            ->andWhere('o.valide = 1')
            ->andWhere('i.alt <> :img')
            ->setParameter('id', $id)
            ->setParameter('img', 'default.png')
            ->setMaxResults(1)
            ->getQuery()
            ->getResult();
    }

    /**
     * @return array
     */
    public function allObs()
    {
        return $this->createQueryBuilder('o')
            ->leftJoin('o.species', 't')
            ->addSelect('t')
            ->where('o.gpsAffiche = 1')
            ->andWhere('o.valide = 1')
            ->getQuery()
            ->getResult();
    }

    /**
     * @param int $page
     * @param int $max
     * @return Paginator
     * @throws NotFoundHttpException
     */
    public function findListValidation($page = 1, $max = 8)
    {
        if(!is_numeric($page)) {
            throw new \InvalidArgumentException(
                '$page must be an integer ('.gettype($page).' : '.$page.')'
            );
        }

        if(!is_numeric($page)) {
            throw new \InvalidArgumentException(
                '$max must be an integer ('.gettype($max).' : '.$max.')'
            );
        }

        $dql = $this->createQueryBuilder('o');
        $dql->where('o.valide = 0');
        $dql->andWhere('o.afficheValide = 1');
        $dql->orderBy('o.id', 'DESC');

        $firstResult = ($page - 1) * $max;

        $query = $dql->getQuery();
        $query->setFirstResult($firstResult);
        $query->setMaxResults($max);

        $paginator = new Paginator($query);

        if(($paginator->count() <=  $firstResult) && $page != 1) {
            throw new NotFoundHttpException('Page not found');
        }

        return $paginator;
    }

    /**
     * @param $id
     * @return array
     */
    public function userObservationValide($id)
    {
        return $this->createQueryBuilder('o')
            ->select('o')
            ->where('o.user = :id')
            ->andWhere('o.valide = 1')
            ->setParameter('id', $id)
            ->getQuery()
            ->getResult();
    }

    public function userObservationAttente($id)
    {
        return $this->createQueryBuilder('o')
            ->select('o')
            ->leftJoin('o.image', 'i')
            ->addSelect('i')
            ->where('o.user = :id')
            ->andWhere('o.valide = 0')
            ->setParameter('id', $id)
            ->getQuery()
            ->getResult();
    }

    /**
     * @return array
     */
    public function imageJour()
    {
        return $this->createQueryBuilder('o')
            ->leftJoin('o.image', 'i')
            ->addSelect('i')
            ->where('o.valide = 1')
            ->andWhere('i.alt IS NOT NULL')
            ->addOrderBy('o.date','DESC')
            ->setMaxResults(8)
            ->getQuery()
            ->getResult();
    }

    /**
     * @return mixed
     */
    public function imageJourCount()
    {
        return $this->createQueryBuilder('o')
            ->select('COUNT(o)')
            ->where('o.valide = 1')
            ->getQuery()
            ->getSingleScalarResult();
    }


}