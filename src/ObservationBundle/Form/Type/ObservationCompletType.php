<?php
/**
 * Created by PhpStorm.
 * User: Jeranders
 * Date: 02/02/2017
 * Time: 18:04
 */

namespace ObservationBundle\Form\Type;

use ImportBundle\Repository\TaxrefRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\All;
use Symfony\Component\Validator\Constraints\Image;
use Vich\UploaderBundle\Form\Type\VichImageType;

class ObservationCompletType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('date', DateType::class, array(
                'widget' => 'single_text',
                'html5' => false,
                'attr' => array(
                    'placeholder' => 'Choisir la date de l\'observation au format aaaa-mm-jj',
                )
            ))
            ->add('latitude', TextType::class, array(
                'attr' => array(
                    'placeholder' => 'Latitude exemple : 50.5365448',
                    'pattern' => '^[-+]?([1-8]?\d(\.\d+)?|90(\.0+)?)$',
                    'message' => 'Les Coordonnées sont incorrects'
                )
            ))
            ->add('longitude', TextType::class, array(
                'attr' => array(
                    'placeholder' => 'Longitude exemple : 14.75555',
                    'pattern' => '^[-+]?(180(\.0+)?|((1[0-7]\d)|([1-9]?\d))(\.\d+)?)$',
                    'message' => 'Les Coordonnées sont incorrects'
                )
            ))
            ->add('nombre', IntegerType::class)

            ->add('image', ImageType::class, array(
                'required' => false,
                'label' => 'Image'
            ))
            ->add('valide', HiddenType::class)
            ->add('meteo', ChoiceType::class, array(
                'choices' => array(
                    'Brouillard' => 'Brouillard',
                    'Canicule' => 'Canicule',
                    'Ensoleillé' => 'Ensoleille',
                    'Gel' => 'Gel',
                    'Mitigé' => 'Mitige',
                    'Précipitation' => 'Precipitation',
                    'Temps gris' => 'Temps gris',
                    'Vent' => 'Vent'
                )
            ))
            ->add('saison', ChoiceType::class, array(
                'choices' => array('Printemps' => 'Printemps', 'Été' => 'Ete', 'Automne' => 'Automne', 'Hiver' => 'Hiver')
            ))
            ->add('typeSaisie', HiddenType::class)
            ->add('precipitation', ChoiceType::class, array(
                'choices' => array(
                    'Aucune' => 'Aucune',
                    'Averse' => 'Averse',
                    'Bruine' => 'Bruine',
                    'Grêle' => 'Grele',
                    'Grésil' => 'Gresil',
                    'Neige' => 'Neige',
                    'Orage' => 'Orage',
                    'Pluie' => 'Pluie',
                    'Pluie torrentielle / Averse violente' => "Pluie torrentielle",
                    'Pluie verglaçante' => "Pluie verglaçante"
                )
            ))
            ->add('periode', ChoiceType::class, array(
                'choices' => array(
                    'Aube' => 'Aube',
                    'Matin' => 'Matin',
                    'Après-midi' => "Apres-midi",
                    'Crépuscule' => 'Crepuscule',
                    'Soirée' => 'Soirée',
                    'Nuit' => 'Nuit'
                )
            ))
            ->add('environnement', ChoiceType::class, array(
                'choices' => array(
                    'Bord de fleuve' => 'fleuve',
                    'Bord de mer' => 'mer',
                    'Bord de rivière' => 'riviere',
                    'Champs' => 'champs',
                    'Fôret' => 'foret',
                    'Grande ville' => 'ville',
                    'Lac' => 'lac',
                    'Montagne' => 'montagne',
                    'Parc municipal' => 'parc',
                    'Plaine' => 'plaine',
                    'Village' => 'village'
                )
            ))
            ->add('sensibilite', ChoiceType::class, array(
                'choices' => array('Protégé' => 'protege', 'Non protégé' => 'non protege', 'Je ne sais pas' => 'ne sais pas')
            ))
            ->add('comportement', ChoiceType::class, array(
                'choices' => array(
                    'Chante' => 'chante',
                    'Construit son nid' => 'nid',
                    'Fait sa toilette' => 'toilette',
                    'Inactif' => 'inactif',
                    'Mange' => 'mange',
                    'Posé' => 'pose',
                    "Vole" => 'vole'
                )
            ))
            ->add('species', EntityType::class, array(
                'label' => 'Espèce observée :',
                'class' => 'ImportBundle\Entity\Taxref',
                'choice_label' => 'nomVern',
                'query_builder' => function (TaxrefRepository $er) {
                    return $er->createQueryBuilder('t')
                        ->orderBy('t.nomVern', 'ASC');
                },
            ))
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'ObservationBundle\Entity\Observation'
        ));
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'observationbundle_observation';
    }
}