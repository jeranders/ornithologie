<?php

namespace ObservationBundle\Form\Type;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\DateTime;
use ImportBundle\Repository\TaxrefRepository;
use ImportBundle\Entity\Taxref;

class ObservationType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('date', DateType::class, array(
                'widget' => 'single_text',
                'html5' => false,
                'attr' => array(
                    'placeholder' => 'Choisir la date de l\'observation au format aaaa-mm-jj',
                )
            ))
            ->add('latitude', TextType::class, array(
                'attr' => array(
                    'placeholder' => 'Latitude exemple : 50.5365448',
                    'pattern' => '^[-+]?([1-8]?\d(\.\d+)?|90(\.0+)?)$',
                    'message' => 'Les Coordonnées sont incorrects'
                )
            ))
            ->add('longitude', TextType::class, array(
                'attr' => array(
                    'placeholder' => 'Longitude exemple : 14.75555',
                    'pattern' => '^[-+]?(180(\.0+)?|((1[0-7]\d)|([1-9]?\d))(\.\d+)?)$',
                    'message' => 'Les Coordonnées sont incorrects'
                )
            ))
            ->add('nombre', IntegerType::class)
            ->add('image', ImageType::class, array(
                'required' => false,
                'label' => 'Image'
            ))
            ->add('valide', HiddenType::class)
            ->add('commentaire', HiddenType::class)
            ->add('gpsAffiche', HiddenType::class)
            ->add('meteo', HiddenType::class)
            ->add('saison', HiddenType::class)
            ->add('typeSaisie', HiddenType::class)
            ->add('precipitation', HiddenType::class)
            ->add('periode', HiddenType::class)
            ->add('environnement', HiddenType::class)
            ->add('sensibilite', HiddenType::class)
            ->add('comportement', HiddenType::class)
            ->add('species', EntityType::class, array(
                'label' => 'Espèce observée :',
                'class' => 'ImportBundle\Entity\Taxref',
                'choice_label' => 'nomVern',
                'query_builder' => function (TaxrefRepository $er) {
                    return $er->createQueryBuilder('t')
                        ->orderBy('t.nomVern', 'ASC');
                },
            ))
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'ObservationBundle\Entity\Observation'
        ));
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'observationbundle_observation';
    }


}