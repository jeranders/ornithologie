<?php
/**
 * Created by PhpStorm.
 * User: Moltes
 * Date: 07/07/2017
 * Time: 21:44
 */

namespace ObservationBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class ImageType
 * @package ObservationBundle\Form
 */
class ImageType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('file', FileType::class, array(
                'label' => false,
                'attr' => array(
                    'accept' => 'image/*',
                    'capture' => '',
                ),
                'constraints' => new Assert\Image(),
            ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'ObservationBundle\Entity\Image'
        ));
    }

    public function getBlockPrefix()
    {
        return 'observation_bundle_image_type';
    }
}