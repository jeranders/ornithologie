<?php

namespace ObservationBundle\Entity;

use ImportBundle\ImportBundle;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * Observation
 *
 * @ORM\Table(name="observations")
 * @ORM\Entity(repositoryClass="ObservationBundle\Repository\ObservationRepository")
 */
class Observation
{
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="ImportBundle\Entity\Taxref")
     * @ORM\JoinColumn(nullable=false)
     * @Assert\NotBlank()
     */
    private $species;

    /**
     * @ORM\Column(name="observation_date", type="datetime")
     * @Assert\DateTime(
     *     format="yyyy-MM-dd",
     *     message="Format de la date incorrect"
     * )
     * @Assert\Range(
     *     max="now",
     *     maxMessage="Attention  la date est supérieure a celle du jour"
     * )
     */
    private $date;

    /**
     * @ORM\Column(name="latitude", type="string")
     * @Assert\NotBlank()
     */
    private $latitude;

    /**
     * @ORM\Column(name="longitude", type="string")
     * @Assert\NotBlank()
     */
    private $longitude;

    /**
     * @ORM\Column(name="nombre", type="integer")
     * @Assert\NotBlank()
     */
    private $nombre;

    /**
     * @ORM\OneToOne(targetEntity="ObservationBundle\Entity\Image", cascade={"persist", "remove"})     *
     */
    private $image;

    /**
     * @ORM\Column(type="datetime")
     *
     * @var \DateTime
     */
    private $updatedAt;

    /**
     * @ORM\Column(name="valide", type="integer")
     */
    private $valide;

    /**
     * @ORM\ManyToOne(targetEntity="UserBundle\Entity\User")
     * @ORM\JoinColumn(nullable=false, referencedColumnName="id",onDelete="cascade")
     */
    private $user;

    /**
     * @ORM\Column(name="saison", type="string", nullable = true)
     */
    private $saison;

    /**
     * @ORM\Column(name="precipitation", type="string", nullable = true)
     */
    private $precipitation;

    /**
     * @ORM\Column(name="meteo", type="string", nullable = true)
     */
    private $meteo;

    /**
     * @ORM\Column(name="periode", type="string", nullable = true)
     */
    private $periode;

    /**
     * @ORM\Column(name="environnement", type="string", nullable = true)
     */
    private $environnement;

    /**
     * @ORM\Column(name="comportement", type="string", nullable = true)
     */
    private $comportement;

    /**
     * @ORM\Column(name="sensibilite", type="string", nullable = true)
     */
    private $sensibilite;

    /**
     * @ORM\Column(name="type_saisie", type="integer")
     */
    private $typeSaisie;

    /**
     * @ORM\Column(name="commentaire", type="text", nullable = true)
     */
    private $commentaire;

    /**
     * @ORM\Column(name="gps_affiche", type="integer")
     */
    private $gpsAffiche;

    /**
     * @ORM\Column(name="affiche_valide", type="integer")
     */
    private $afficheValide;

    public function __construct()
    {
        $this->gpsAffiche = '1';
        $this->afficheValide = '1';
        $this->updatedAt = new \DateTime();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getSpecies()
    {
        return $this->species;
    }

    /**
     * @param mixed $species
     */
    public function setSpecies($species)
    {
        $this->species = $species;
    }

    /**
     * @return mixed
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param mixed $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }

    /**
     * @return mixed
     */
    public function getLatitude()
    {
        return $this->latitude;
    }

    /**
     * @param mixed $latitude
     */
    public function setLatitude($latitude)
    {
        $this->latitude = $latitude;
    }

    /**
     * @return mixed
     */
    public function getLongitude()
    {
        return $this->longitude;
    }

    /**
     * @param mixed $longitude
     */
    public function setLongitude($longitude)
    {
        $this->longitude = $longitude;
    }

    /**
     * @return mixed
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * @param mixed $nombre
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    }

    /**
     * @return mixed
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param mixed $image
     */
    public function setImage($image)
    {
        $this->image = $image;
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param \DateTime $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * @return mixed
     */
    public function getValide()
    {
        return $this->valide;
    }

    /**
     * @param mixed $valide
     */
    public function setValide($valide)
    {
        $this->valide = $valide;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * @return mixed
     */
    public function getSaison()
    {
        return $this->saison;
    }

    /**
     * @param mixed $saison
     */
    public function setSaison($saison)
    {
        $this->saison = $saison;
    }

    /**
     * @return mixed
     */
    public function getPrecipitation()
    {
        return $this->precipitation;
    }

    /**
     * @param mixed $precipitation
     */
    public function setPrecipitation($precipitation)
    {
        $this->precipitation = $precipitation;
    }

    /**
     * @return mixed
     */
    public function getMeteo()
    {
        return $this->meteo;
    }

    /**
     * @param mixed $meteo
     */
    public function setMeteo($meteo)
    {
        $this->meteo = $meteo;
    }

    /**
     * @return mixed
     */
    public function getPeriode()
    {
        return $this->periode;
    }

    /**
     * @param mixed $periode
     */
    public function setPeriode($periode)
    {
        $this->periode = $periode;
    }

    /**
     * @return mixed
     */
    public function getEnvironnement()
    {
        return $this->environnement;
    }

    /**
     * @param mixed $environnement
     */
    public function setEnvironnement($environnement)
    {
        $this->environnement = $environnement;
    }

    /**
     * @return mixed
     */
    public function getComportement()
    {
        return $this->comportement;
    }

    /**
     * @param mixed $comportement
     */
    public function setComportement($comportement)
    {
        $this->comportement = $comportement;
    }

    /**
     * @return mixed
     */
    public function getSensibilite()
    {
        return $this->sensibilite;
    }

    /**
     * @param mixed $sensibilite
     */
    public function setSensibilite($sensibilite)
    {
        $this->sensibilite = $sensibilite;
    }

    /**
     * @return mixed
     */
    public function getTypeSaisie()
    {
        return $this->typeSaisie;
    }

    /**
     * @param mixed $typeSaisie
     */
    public function setTypeSaisie($typeSaisie)
    {
        $this->typeSaisie = $typeSaisie;
    }

    /**
     * @return mixed
     */
    public function getCommentaire()
    {
        return $this->commentaire;
    }

    /**
     * @param mixed $commentaire
     */
    public function setCommentaire($commentaire)
    {
        $this->commentaire = $commentaire;
    }

    /**
     * @return mixed
     */
    public function getGpsAffiche()
    {
        return $this->gpsAffiche;
    }

    /**
     * @param mixed $gpsAffiche
     */
    public function setGpsAffiche($gpsAffiche)
    {
        $this->gpsAffiche = $gpsAffiche;
    }

    /**
     * @return mixed
     */
    public function getAfficheValide()
    {
        return $this->afficheValide;
    }

    /**
     * @param mixed $afficheValide
     */
    public function setAfficheValide($afficheValide)
    {
        $this->afficheValide = $afficheValide;
    }


}