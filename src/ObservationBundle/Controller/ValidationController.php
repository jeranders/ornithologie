<?php
/**
 * Author: Jérôme Brechoire
 * Email: brechoire.j@gmail.com
 */

namespace ObservationBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class ValidationController extends Controller
{
    /**
     * @param Request $request
     * @param $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function validationAction(Request $request, $id)
    {
        $obs = $this->get('app.observation')->observation($id);
        $validation =$this->get('app.observation')->validationObservation($request, $id);
        if ($validation->isSubmitted()) {
            return $this->redirectToRoute('liste_validation');
        }
        return $this->render('default/validations_saisies.html.twig', array(
            'obs' => $obs,
            'form' => $validation->createView()
        ));
    }
}