<?php
/**
 * Author: Jérôme Brechoire
 * Email: brechoire.j@gmail.com
 */

namespace ObservationBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class SaisieCompleteController
 * @package ObservationBundle\Controller
 */
class SaisieCompleteController extends Controller
{
    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function saisieCompleteAction(Request $request)
    {
        $formComplet = $this->get('app.observation')->saisieObservationComplet($request);
        if ($formComplet->isSubmitted() && $formComplet->isValid()) {
            return $this->redirectToRoute('saisieComplete');
        }
        return $this->render('default/saisir_observations_complet.html.twig', array(
            'formComplet' => $formComplet->createView()
        ));
    }
}