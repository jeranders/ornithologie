<?php
/**
 * Author: Jérôme Brechoire
 * Email: brechoire.j@gmail.com
 */

namespace ObservationBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class ListeValidationController extends Controller
{
    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function listeValidationAction(Request $request)
    {
        $validation = $this->get('app.observation')
            ->listeEspeceValidation($request->query->getInt('page', 1),8);

        return $this->render('default/liste_validations_saisies.html.twig', array(
            'observations' => $validation
        ));
    }
}