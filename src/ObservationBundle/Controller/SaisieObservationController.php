<?php
/**
 * Author: Jérôme Brechoire
 * Email: brechoire.j@gmail.com
 */

namespace ObservationBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class SaisieObservationController
 * @package ObservationBundle\Controller
 */
class SaisieObservationController extends Controller
{
    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function saisieObservationAction(Request $request)
    {
        $formRapide = $this->get('app.observation')->saisieObservationRapide($request);

        if ($formRapide->isSubmitted() && $formRapide->isValid()) {
            return $this->redirectToRoute('saisie_observation');
        }

        return $this->render('default/saisir_observations.html.twig', array(
            'form' => $formRapide->createView()
        ));
    }
}