<?php

namespace ObservationBundle\Service;

use Doctrine\ORM\EntityManager;
use ImportBundle\Entity\Taxref;
use ObservationBundle\Entity\Image;
use ObservationBundle\Form\Type\ObservationCompletType;
use ObservationBundle\Form\Type\ValidationType;
use ObservationBundle\Repository\ObservationRepository;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use UserBundle\Entity\User;
use ObservationBundle\Entity\Observation;
use ObservationBundle\Form\Type\ObservationType;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use UserBundle\UserBundle;

class ObservationService
{

    /**
     * @var EntityManager
     */
    private $doctrine;

    /**
     * @var FormFactory
     */
    private $form;

    /**
     * @var TokenStorage
     */
    private $security;

    /**
     * @var Session
     */
    private $session;

    private $repoTaxref;

    private $secuCheck;

    /**
     * @var FileUploader
     */
    private $fileuploader;

    private $repoImage;

    /**
     * ObservationService constructor.
     * @param EntityManager $doctrine
     * @param FormFactory $form
     * @param $security
     * @param Session $session
     * @param $repoTaxref
     * @param $secuCheck
     * @param FileUploader $fileuploader
     */
    public function __construct(EntityManager $doctrine, FormFactory $form, $security, Session $session, $repoTaxref, $secuCheck, FileUploader $fileuploader, $repoImage)
    {
        $this->doctrine = $doctrine;
        $this->form = $form;
        $this->security = $security;
        $this->session = $session;
        $this->repoTaxref = $repoTaxref;
        $this->secuCheck = $secuCheck;
        $this->fileuploader = $fileuploader;
        $this->repoImage = $repoImage;
    }

    /**
     * @return mixed
     */
    public function findTaxref()
    {
        $count = count($this->doctrine->getRepository('ImportBundle:Taxref')->findBy(array()));
        return $count;
    }

    /**
     * Traitement formulare saisie rapide
     *
     * @param Request $request
     * @return \Symfony\Component\Form\FormInterface
     */
    public function saisieObservationRapide(Request $request)
    {
        $observation = new Observation();

        $form = $this->form->create(ObservationType::class, $observation);

        $user = $this->security->getToken()->getUser();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid())
        {
            if ($user != null)
            {
                // Si l'utilisateur est un naturaliste l'observation est validé
                if ($this->secuCheck->isGranted('ROLE_NATURALISTE')) {
                    $observation->setValide('1');
                    $observation->setAfficheValide('0');
                }else{
                    $observation->setValide('0');
                }

                $form->getData();
                $img = $file = $form['image']->getData();

                // Si une image est envoyé (data non vide), on l'upload
                if ($img != null){
                    $file = $form['image']->getData()->getFile();
                    $fileName = $this->fileuploader->upload($file);
                    // Ajoute le nom de l'image (rand) dans le setter
                    $observation->getImage()->setAlt($fileName);
                    // Ajoute l'id du specie
                    $specie = $form['species']->getData();
                    $observation->getImage()->setSpecies($specie);
                }

                $observation->setTypeSaisie("0");
                $observation->setUser($user);
                $this->doctrine->persist($observation);
                $this->doctrine->flush();
                $observation->getId();

                /**
                 * Permets l'ajout d'une image dans l'entité Taxref
                 */
                if ($img != null){
                    if ($this->secuCheck->isGranted('ROLE_NATURALISTE')) {
                        $this->updateTaxrefImage($observation->getSpecies(), $file, $fileName);
                    }
                }

                $this->session->getFlashBag()->add('notice', 'Merci pour votre observation rapide');

            }

        }

        return $form;
    }

    /**
     * @param $id
     * @param $file
     * @param $lastImage
     */
    public function updateTaxrefImage($id, $file, $lastImage)
    {
        $findTaxref = $this->doctrine->getRepository(Taxref::class)->find($id);
        if ($findTaxref->getImage() == null && $file != null)
        {

            $findTaxref->setImage($lastImage);
            dump($findTaxref->setImage($lastImage));

            $this->doctrine->flush();
            dump($lastImage);
        }
    }

    /**
     * Traitement formulaire saisie complète
     * @param Request $request
     * @return \Symfony\Component\Form\FormInterface
     */
    public function saisieObservationComplet(Request $request)
    {
        $observation = new Observation();

        $form = $this->form->create(ObservationCompletType::class, $observation);

        $user = $this->security->getToken()->getUser();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid())
        {
            if ($user != null)
            {
                if ($this->secuCheck->isGranted('ROLE_NATURALISTE')) {
                    $observation->setValide('1');
                    $observation->setAfficheValide('0');
                }else{
                    $observation->setValide('0');
                }

                $form->getData();
                $img = $file = $form['image']->getData();

                // Si une image est envoyé (data non vide), on l'upload
                if ($img != null){
                    $file = $form['image']->getData()->getFile();
                    $fileName = $this->fileuploader->upload($file);
                    // Ajoute le nom de l'image (rand) dans le setter
                    $observation->getImage()->setAlt($fileName);
                    // Ajoute l'id du specie
                    $specie = $form['species']->getData();
                    $observation->getImage()->setSpecies($specie);
                }

                $observation->setTypeSaisie("1");
                $observation->setUser($user);
                $this->doctrine->persist($observation);
                $this->doctrine->flush();

                /**
                 * Update Taxref
                 */
                if ($img != null){
                    if ($this->secuCheck->isGranted('ROLE_NATURALISTE')) {
                        $this->updateTaxrefImage($observation->getSpecies(), $file, $fileName);
                    }
                }

                $this->session->getFlashBag()->add('notice', 'Merci pour votre observation');

            }
        }

        return $form;
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Symfony\Component\Form\FormInterface
     */
    public function validationObservation(Request $request, $id)
    {

        $t = $this->observation($id);

        $form = $this->form->create(ValidationType::class, $t);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid())
        {

            $form->getData();
            $valide = $t->getValide();

            $t->setAfficheValide('0');

            if ($valide == 1){
                $this->session->getFlashBag()->add('notice', 'Validation ok');
            }else{
                $this->session->getFlashBag()->add('notice', 'Validation refusée');
            }

            $this->doctrine->flush();

        }

        return $form;
    }

    /**
     * Récupération de la liste des observations
     * @return array|\ObservationBundle\Entity\Observation[]
     */
    public function listeObservation()
    {
        $liste = $this->doctrine->getRepository('ObservationBundle:Observation')->findAll();
        return $liste;
    }

    /**
     * Recherche une observation via l'id
     * @param $id
     * @return Observation
     */
    public function observation($id)
    {
        $obs = $this->doctrine->getRepository('ObservationBundle:Observation')->find($id);
        return $obs;
    }

    /**
     * @param $param
     * @return array
     */
    public function listeEspece($param)
    {
        $db = $this->doctrine
            ->getRepository('ImportBundle:Taxref')
            ->findByPage($param);
        $image = $this->doctrine->getRepository(Taxref::class)->imageSpecie();

        return array('taxref' => $db, 'img' => $image);

    }

    /**
     * @param $param
     * @return \Doctrine\ORM\Tools\Pagination\Paginator
     */
    public function listeEspeceValidation($param)
    {
        $db = $this->doctrine
            ->getRepository('ObservationBundle:Observation')
            ->findListValidation($param);
        return $db;

    }

    /**
     * Compte les informations (nombre user, observations...)
     * @return array
     */
    public function countInfo()
    {
        $countObs = $this->doctrine->getRepository('ObservationBundle:Observation')->countAllObservation();
        $countValide = $this->doctrine->getRepository('ObservationBundle:Observation')->countValideObservation();
        $countUser = $this->doctrine->getRepository('UserBundle:User')->countUser();
        $countGpsValide = $this->doctrine->getRepository('ObservationBundle:Observation')->afficheGPS();

        return array(
            'countObs' => $countObs,
            'countValide' => $countValide,
            'countUser' => $countUser,
            'countGpsValide' => $countGpsValide
        );
    }

    /**
     * @return mixed
     */
    public function afficheGpsService()
    {
        $countGpsValide = $this->doctrine->getRepository('ObservationBundle:Observation')->afficheGPS();
        return $countGpsValide;
    }

    /**
     * @param $id
     * @return array
     */
    public function ficheEspece($id)
    {
        $fiche = $this->doctrine->getRepository('ImportBundle:Taxref')->find($id);
        $obs = $this->doctrine->getRepository('ObservationBundle:Observation')->lastArraySaisie($id);
        $count = $this->doctrine->getRepository('ObservationBundle:Observation')->countObservationEspece($id);
        $coordonnees = $this->doctrine->getRepository('ObservationBundle:Observation')->listMapObservation($id);
        $one = $this->doctrine->getRepository('ObservationBundle:Observation')->oneImage($id);
        return array(
            'count' => $count,
            'fiche' => $fiche,
            'obs' => $obs,
            'coordonnees' => $coordonnees,
            'one' => $one
        );
    }

    /**
     * @return array
     */
    public function carnetTerrain()
    {
        $user = $this->security->getToken()->getUser();
        $saisieValide = $this->doctrine->getRepository('ObservationBundle:Observation')->userObservationValide($user);
        $saisieAttente = $this->doctrine->getRepository('ObservationBundle:Observation')->userObservationAttente($user);
        $profil = $this->doctrine->getRepository('UserBundle:User')->find($user);

        return array(
            'profil' => $profil,
            'saisieValide' => $saisieValide,
            'saisieAttente' => $saisieAttente
        );
    }

    /**
     * @return array
     */
    public function imageJour()
    {
        $imageJour = $this->doctrine->getRepository('ObservationBundle:Observation')->imageJour();
        $imageJourCount = $this->doctrine->getRepository('ObservationBundle:Observation')->imageJourCount();

        return array(
            'imageJourListe' => $imageJour,
            'imageJourCount' => $imageJourCount
        );
    }

    /**
     * @return array
     */
    public function allObsMap()
    {
        $obsCoo = $this->doctrine->getRepository('ObservationBundle:Observation')->allObs();

        return $obsCoo;
    }

    /**
     * @return array
     */
    public function lastObsCarte()
    {
        $lastObs = $this->doctrine->getRepository('ObservationBundle:Observation')->lastObs();

        return $lastObs;
    }


}