<?php

namespace ImportBundle\Repository;

use Doctrine\DBAL\Query\QueryBuilder;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;
use ObservationBundle\Entity\Image;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class TaxrefRepository extends EntityRepository
{
    /**
     * Pagination liste des especes
     * @param int $page
     * @param int $max
     * @return Paginator
     */
    public function findByPage($page = 1, $max = 8)
    {
        if(!is_numeric($page)) {
            throw new \InvalidArgumentException(
                '$page must be an integer ('.gettype($page).' : '.$page.')'
            );
        }

        if(!is_numeric($page)) {
            throw new \InvalidArgumentException(
                '$max must be an integer ('.gettype($max).' : '.$max.')'
            );
        }

        $dql = $this->createQueryBuilder('t');
        $dql->orderBy('t.id', 'DESC');

        $firstResult = ($page - 1) * $max;

        $query = $dql->getQuery();
        $query->setFirstResult($firstResult);
        $query->setMaxResults($max);

        $paginator = new Paginator($query);

        if(($paginator->count() <=  $firstResult) && $page != 1) {
            throw new NotFoundHttpException('Page not found');
        }

        return $paginator;
    }

    /**
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function distinctTaxref()
    {
        return $this
            ->createQueryBuilder('t')
            ->select('t.nomVern')->distinct()
            ->orderBy('t.nomVern', 'ASC');

    }

    /**
     * @param $search
     * @return array
     */
    public function searchRepository($search)
    {
        return $this->createQueryBuilder('t')
            ->select('t.nomVern')
            ->addSelect('t.nomValide')
            ->addSelect('t.id')
            ->andWhere('t.nomVern LIKE :search OR t.nomValide LIKE :search')
            ->setParameter('search', '%' . $search . '%')
            ->getQuery()
            ->getResult();
    }

    /**
     * @param $search
     * @return mixed
     */
    public function searchCountRepository($search)
    {
        return $this->createQueryBuilder('t')
            ->select('count(t)')
            ->andWhere('t.nomVern LIKE :search OR t.nomValide LIKE :search')
            ->setParameter('search', '%' . $search . '%')
            ->getQuery()
            ->getSingleScalarResult();
    }

    public function imageSpecie()
    {
        return $this->createQueryBuilder('t')
            ->select('t')
            ->leftJoin(Image::class, 'i')
            ->where('t.image = i.id')
            ->getQuery()
            ->getResult();
    }
}