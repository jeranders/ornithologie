<?php


namespace ImportBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use ObservationBundle\Entity\Image;


/**
 * Taxref
 *
 * NAO table taxref
 *
 * @ORM\Table(name="taxref")
 * @ORM\Entity(repositoryClass="ImportBundle\Repository\TaxrefRepository")
 */
class Taxref
{
    /**
     * @var string
     *
     * @ORM\Column(name="REGNE", type="string", length=8, nullable=true)
     */
    private $regne;
    /**
     * @var string
     *
     * @ORM\Column(name="PHYLUM", type="string", length=8, nullable=true)
     */
    private $phylum;
    /**
     * @var string
     *
     * @ORM\Column(name="CLASSE", type="string", length=4, nullable=true)
     */
    private $classe;
    /**
     * @var string
     *
     * @ORM\Column(name="ORDRE", type="string", length=19, nullable=true)
     */
    private $ordre;
    /**
     * @var string
     *
     * @ORM\Column(name="FAMILLE", type="string", length=17, nullable=true)
     */
    private $famille;
    /**
     * @var integer
     *
     * @ORM\Column(name="CD_NOM", type="integer", nullable=true)
     */
    private $cdNom;
    /**
     * @var integer
     *
     * @ORM\Column(name="CD_TAXSUP", type="integer", nullable=true)
     */
    private $cdTaxsup;
    /**
     * @var integer
     *
     * @ORM\Column(name="CD_REF", type="integer", nullable=true)
     */
    private $cdRef;
    /**
     * @var string
     *
     * @ORM\Column(name="RANG", type="string", length=4, nullable=true)
     */
    private $rang;
    /**
     * @var string
     *
     * @ORM\Column(name="LB_NOM", type="string", length=47, nullable=true)
     */
    private $lbNom;
    /**
     * @var string
     *
     * @ORM\Column(name="LB_AUTEUR", type="string", length=51, nullable=true)
     */
    private $lbAuteur;
    /**
     * @var string
     *
     * @ORM\Column(name="NOM_COMPLET", type="string", length=83, nullable=true)
     */
    private $nomComplet;
    /**
     * @var string
     *
     * @ORM\Column(name="NOM_VALIDE", type="string", length=83, nullable=true)
     */
    private $nomValide;
    /**
     * @var string
     *
     * @ORM\Column(name="NOM_VERN", type="string", length=101, nullable=true)
     */
    private $nomVern;
    /**
     * @var string
     *
     * @ORM\Column(name="NOM_VERN_ENG", type="string", length=122, nullable=true)
     */
    private $nomVernEng;
    /**
     * @var integer
     *
     * @ORM\Column(name="HABITAT", type="integer", nullable=true)
     */
    private $habitat;
    /**
     * @var string
     *
     * @ORM\Column(name="FR", type="string", length=1, nullable=true)
     */
    private $fr;
    /**
     * @var string
     *
     * @ORM\Column(name="GF", type="string", length=1, nullable=true)
     */
    private $gf;
    /**
     * @var string
     *
     * @ORM\Column(name="MAR", type="string", length=1, nullable=true)
     */
    private $mar;
    /**
     * @var string
     *
     * @ORM\Column(name="GUA", type="string", length=1, nullable=true)
     */
    private $gua;
    /**
     * @var string
     *
     * @ORM\Column(name="SM", type="string", length=1, nullable=true)
     */
    private $sm;
    /**
     * @var string
     *
     * @ORM\Column(name="SB", type="string", length=1, nullable=true)
     */
    private $sb;
    /**
     * @var string
     *
     * @ORM\Column(name="SPM", type="string", length=1, nullable=true)
     */
    private $spm;
    /**
     * @var string
     *
     * @ORM\Column(name="MAY", type="string", length=1, nullable=true)
     */
    private $may;
    /**
     * @var string
     *
     * @ORM\Column(name="EPA", type="string", length=1, nullable=true)
     */
    private $epa;
    /**
     * @var string
     *
     * @ORM\Column(name="REU", type="string", length=1, nullable=true)
     */
    private $reu;
    /**
     * @var string
     *
     * @ORM\Column(name="SA", type="string", length=1, nullable=true)
     */
    private $sa;
    /**
     * @var string
     *
     * @ORM\Column(name="TA", type="string", length=1, nullable=true)
     */
    private $ta;
    /**
     * @var string
     *
     * @ORM\Column(name="TAAF", type="string", length=1, nullable=true)
     */
    private $taaf;
    /**
     * @var string
     *
     * @ORM\Column(name="NC", type="string", length=1, nullable=true)
     */
    private $nc;
    /**
     * @var string
     *
     * @ORM\Column(name="WF", type="string", length=1, nullable=true)
     */
    private $wf;
    /**
     * @var string
     *
     * @ORM\Column(name="PF", type="string", length=1, nullable=true)
     */
    private $pf;
    /**
     * @var string
     *
     * @ORM\Column(name="CLI", type="string", length=1, nullable=true)
     */
    private $cli;
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="image", type="string", nullable=true)
     */
    private $image;

    /**
     * @return string
     */
    public function getGf()
    {
        return $this->gf;
    }

    /**
     * @param string $gf
     */
    public function setGf($gf)
    {
        $this->gf = $gf;
    }

    /**
     * @return string
     */
    public function getRegne()
    {
        return $this->regne;
    }

    /**
     * @param string $regne
     */
    public function setRegne($regne)
    {
        $this->regne = $regne;
    }

    /**
     * @return string
     */
    public function getPhylum()
    {
        return $this->phylum;
    }

    /**
     * @param string $phylum
     */
    public function setPhylum($phylum)
    {
        $this->phylum = $phylum;
    }

    /**
     * @return string
     */
    public function getClasse()
    {
        return $this->classe;
    }

    /**
     * @param string $classe
     */
    public function setClasse($classe)
    {
        $this->classe = $classe;
    }

    /**
     * @return string
     */
    public function getOrdre()
    {
        return $this->ordre;
    }

    /**
     * @param string $ordre
     */
    public function setOrdre($ordre)
    {
        $this->ordre = $ordre;
    }

    /**
     * @return string
     */
    public function getFamille()
    {
        return $this->famille;
    }

    /**
     * @param string $famille
     */
    public function setFamille($famille)
    {
        $this->famille = $famille;
    }

    /**
     * @return int
     */
    public function getCdNom()
    {
        return $this->cdNom;
    }

    /**
     * @param int $cdNom
     */
    public function setCdNom($cdNom)
    {
        $this->cdNom = $cdNom;
    }

    /**
     * @return int
     */
    public function getCdTaxsup()
    {
        return $this->cdTaxsup;
    }

    /**
     * @param int $cdTaxsup
     */
    public function setCdTaxsup($cdTaxsup)
    {
        $this->cdTaxsup = $cdTaxsup;
    }

    /**
     * @return int
     */
    public function getCdRef()
    {
        return $this->cdRef;
    }

    /**
     * @param int $cdRef
     */
    public function setCdRef($cdRef)
    {
        $this->cdRef = $cdRef;
    }

    /**
     * @return string
     */
    public function getRang()
    {
        return $this->rang;
    }

    /**
     * @param string $rang
     */
    public function setRang($rang)
    {
        $this->rang = $rang;
    }

    /**
     * @return string
     */
    public function getLbNom()
    {
        return $this->lbNom;
    }

    /**
     * @param string $lbNom
     */
    public function setLbNom($lbNom)
    {
        $this->lbNom = $lbNom;
    }

    /**
     * @return string
     */
    public function getLbAuteur()
    {
        return $this->lbAuteur;
    }

    /**
     * @param string $lbAuteur
     */
    public function setLbAuteur($lbAuteur)
    {
        $this->lbAuteur = $lbAuteur;
    }

    /**
     * @return string
     */
    public function getNomComplet()
    {
        return $this->nomComplet;
    }

    /**
     * @param string $nomComplet
     */
    public function setNomComplet($nomComplet)
    {
        $this->nomComplet = $nomComplet;
    }

    /**
     * @return string
     */
    public function getNomValide()
    {
        return $this->nomValide;
    }

    /**
     * @param string $nomValide
     */
    public function setNomValide($nomValide)
    {
        $this->nomValide = $nomValide;
    }

    /**
     * @return string
     */
    public function getNomVern()
    {
        return $this->nomVern;
    }

    /**
     * @param string $nomVern
     */
    public function setNomVern($nomVern)
    {
        $this->nomVern = $nomVern;
    }

    /**
     * @return string
     */
    public function getNomVernEng()
    {
        return $this->nomVernEng;
    }

    /**
     * @param string $nomVernEng
     */
    public function setNomVernEng($nomVernEng)
    {
        $this->nomVernEng = $nomVernEng;
    }

    /**
     * @return int
     */
    public function getHabitat()
    {
        return $this->habitat;
    }

    /**
     * @param int $habitat
     */
    public function setHabitat($habitat)
    {
        $this->habitat = $habitat;
    }

    /**
     * @return string
     */
    public function getFr()
    {
        return $this->fr;
    }

    /**
     * @param string $fr
     */
    public function setFr($fr)
    {
        $this->fr = $fr;
    }

    /**
     * @return string
     */
    public function getMar()
    {
        return $this->mar;
    }

    /**
     * @param string $mar
     */
    public function setMar($mar)
    {
        $this->mar = $mar;
    }

    /**
     * @return string
     */
    public function getGua()
    {
        return $this->gua;
    }

    /**
     * @param string $gua
     */
    public function setGua($gua)
    {
        $this->gua = $gua;
    }

    /**
     * @return string
     */
    public function getSm()
    {
        return $this->sm;
    }

    /**
     * @param string $sm
     */
    public function setSm($sm)
    {
        $this->sm = $sm;
    }

    /**
     * @return string
     */
    public function getSb()
    {
        return $this->sb;
    }

    /**
     * @param string $sb
     */
    public function setSb($sb)
    {
        $this->sb = $sb;
    }

    /**
     * @return string
     */
    public function getSpm()
    {
        return $this->spm;
    }

    /**
     * @param string $spm
     */
    public function setSpm($spm)
    {
        $this->spm = $spm;
    }

    /**
     * @return string
     */
    public function getMay()
    {
        return $this->may;
    }

    /**
     * @param string $may
     */
    public function setMay($may)
    {
        $this->may = $may;
    }

    /**
     * @return string
     */
    public function getEpa()
    {
        return $this->epa;
    }

    /**
     * @param string $epa
     */
    public function setEpa($epa)
    {
        $this->epa = $epa;
    }

    /**
     * @return string
     */
    public function getReu()
    {
        return $this->reu;
    }

    /**
     * @param string $reu
     */
    public function setReu($reu)
    {
        $this->reu = $reu;
    }

    /**
     * @return string
     */
    public function getSa()
    {
        return $this->sa;
    }

    /**
     * @param string $sa
     */
    public function setSa($sa)
    {
        $this->sa = $sa;
    }

    /**
     * @return string
     */
    public function getTa()
    {
        return $this->ta;
    }

    /**
     * @param string $ta
     */
    public function setTa($ta)
    {
        $this->ta = $ta;
    }

    /**
     * @return string
     */
    public function getTaaf()
    {
        return $this->taaf;
    }

    /**
     * @param string $taaf
     */
    public function setTaaf($taaf)
    {
        $this->taaf = $taaf;
    }

    /**
     * @return string
     */
    public function getNc()
    {
        return $this->nc;
    }

    /**
     * @param string $nc
     */
    public function setNc($nc)
    {
        $this->nc = $nc;
    }

    /**
     * @return string
     */
    public function getWf()
    {
        return $this->wf;
    }

    /**
     * @param string $wf
     */
    public function setWf($wf)
    {
        $this->wf = $wf;
    }

    /**
     * @return string
     */
    public function getPf()
    {
        return $this->pf;
    }

    /**
     * @param string $pf
     */
    public function setPf($pf)
    {
        $this->pf = $pf;
    }

    /**
     * @return string
     */
    public function getCli()
    {
        return $this->cli;
    }

    /**
     * @param string $cli
     */
    public function setCli($cli)
    {
        $this->cli = $cli;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param $image
     */
    public function setImage($image)
    {
        $this->image = $image;
    }

}