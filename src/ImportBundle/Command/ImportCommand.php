<?php
namespace ImportBundle\Command;

use ImportBundle\Entity\Taxref;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Helper\ProgressBar;

/**
 * CSV import file system
 *
 * README
 * To use the import system:
 * In your console ->
 * php bin/console import:csv
 *
 * Class ImportCommand
 * @package ImportBundle\Command
 */
class ImportCommand extends ContainerAwareCommand
{

    protected function configure()
    {
        // Name and description for app/console command
        $this
            ->setName('import:csv')
            ->setDescription('Import taxref from CSV file');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        // Showing when the script is launched
        $now = new \DateTime();
        $output->writeln('<comment>Start : ' . $now->format('d-m-Y G:i:s') . ' ---</comment>');

        // Importing CSV on DB via Doctrine ORM
        $this->import($input, $output);

        // Showing when the script is over
        $now = new \DateTime();
        $output->writeln('<comment>End : ' . $now->format('d-m-Y G:i:s') . ' ---</comment>');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function import(InputInterface $input, OutputInterface $output)
    {
        // Getting php array of data from CSV
        $data = $this->get($input, $output);

        // Getting doctrine manager
        $em = $this->getContainer()->get('doctrine')->getManager();
        // Turning off doctrine default logs queries for saving memory
        $em->getConnection()->getConfiguration()->setSQLLogger(null);

        // Define the size of record, the frequency for persisting the data and the current index of records
        $size = count($data);
        $batchSize = 20;
        $i = 1;

        // Starting progress
        $progress = new ProgressBar($output, $size);
        $progress->start();

        // Processing on each row of data
        foreach($data as $row) {

            $taxref = $em->getRepository('ImportBundle:Taxref')->findAll();

            // If the user doest not exist we create one
            if(!is_object($taxref)){
                $taxref = new Taxref();
                $taxref->setCdNom($row['LB_NOM']);
            }

            // Updating info
            $taxref->setRegne($row['REGNE']);
            $taxref->setPhylum($row['PHYLUM']);
            $taxref->setRegne($row['REGNE']);
            $taxref->setPhylum($row['PHYLUM']);
            $taxref->setClasse($row['CLASSE']);
            $taxref->setOrdre($row['ORDRE']);
            $taxref->setFamille($row['FAMILLE']);
            $taxref->setCdNom($row['CD_NOM']);
            $taxref->setCdTaxsup($row['CD_TAXSUP']);
            $taxref->setCdRef($row['CD_REF']);
            $taxref->setRang($row['RANG']);
            $taxref->setLbNom($row['LB_NOM']);
            $taxref->setLbAuteur($row['LB_AUTEUR']);
            $taxref->setNomComplet($row['NOM_COMPLET']);
            $taxref->setNomValide($row['NOM_VALIDE']);
            $taxref->setNomVern($row['NOM_VERN']);
            $taxref->setNomVernEng($row['NOM_VERN_ENG']);
            $taxref->setHabitat($row['HABITAT']);
            $taxref->setFr($row['FR']);
            $taxref->setGf($row['GF']);
            $taxref->setMar($row['MAR']);
            $taxref->setGua($row['GUA']);
            $taxref->setSm($row['SM']);
            $taxref->setSb($row['SB']);
            $taxref->setSpm($row['SPM']);
            $taxref->setMay($row['MAY']);
            $taxref->setEpa($row['EPA']);
            $taxref->setReu($row['REU']);
            $taxref->setSa($row['SA']);
            $taxref->setTa($row['TA']);
            $taxref->setTaaf($row['TAAF']);
            $taxref->setNc($row['NC']);
            $taxref->setWf($row['WF']);
            $taxref->setPf($row['PF']);
            $taxref->setCli($row['CLI']);

            $em->persist($taxref);

            // Each 20 taxref persisted we flush everything
            if (($i % $batchSize) === 0) {

                $em->flush();
                // Detaches all objects from Doctrine for memory save
                $em->clear();

                // Advancing for progress display on console
                $progress->advance($batchSize);

                $now = new \DateTime();
                $output->writeln(' of taxref imported ... | ' . $now->format('d-m-Y G:i:s'));

            }

            $i++;

        }

        // Flushing and clear data on queue
        $em->flush();
        $em->clear();

        // Ending the progress bar process
        $progress->finish();
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return array|bool
     */
    protected function get(InputInterface $input, OutputInterface $output)
    {
        // Getting the CSV from filesystem (convert utf-8)
        $fileName = 'web/uploads/import/taxref.csv';

        // Using service for converting CSV to PHP Array
        $converter = $this->getContainer()->get('import.csvtoarray');
        $data = $converter->convert($fileName, ';');

        return $data;
    }

}