<?php

namespace AppBundle\Service;

use AppBundle\Form\Type\ContactType;
use Symfony\Bundle\TwigBundle\TwigEngine;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;

class AppService
{
    /**
     * @var FormFactory
     */
    private $form;

    /**
     * @var \Swift_Mailer
     */
    private $mail;

    /**
     * @var TwigEngine
     */
    private $templating;

    /**
     * @var Session
     */
    private $session;

    public function __construct(FormFactory $form, \Swift_Mailer $mail, TwigEngine $templating, Session $session)
    {
        $this->form = $form;
        $this->mail = $mail;
        $this->templating = $templating;
        $this->session = $session;
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\Form\FormInterface
     * @throws \Twig_Error
     */
    public function contact(Request $request)
    {
        $form = $this->form->create(ContactType::class);

        $form->handleRequest($request);

        if ($request->getMethod() == 'POST')
        {
            if ($form->isValid() && $form->isSubmitted())
            {
                $message = \Swift_Message::newInstance()
                    ->setSubject($form['sujet']->getData())
                    ->setFrom($form['email']->getData())
                    ->setTo('agencefineequipe@gmail.com') //REPLACE THIS ADDRESS BY contact@nao.fr
                    ->setBody(
                        $this->templating->render('Email/contact.html.twig', array(
                            'message' => $form['message']->getData(),
                            'nom' => $form['nom']->getData(),
                            'email' => $form['email']->getData(),
                            'telephone' => $form['telephone']->getData(),
                            'sujet' => $form['sujet']->getData()
                        )),
                        'text/html'
                    );
                $this->mail->send($message);
                $this->session->getFlashBag()->add('notice', 'Votre message a bien été envoyé par pigeon voyageur');
            }
        }

        return $form;

    }
}