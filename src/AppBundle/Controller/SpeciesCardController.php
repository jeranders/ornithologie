<?php
/**
 * Author: Jérôme Brechoire
 * Email: brechoire.j@gmail.com
 */

namespace AppBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * Class SpeciesCardController
 * @package AppBundle\Controller
 */
class SpeciesCardController extends Controller
{
    /**
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function speciesCardAction($id)
    {
        $fiche = $this->get('app.observation')->ficheEspece($id);

        return $this->render('default/consultation_fiche.html.twig' ,array(
            'fiche' => $fiche
        ));
    }
}