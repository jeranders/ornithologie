<?php
/**
 * Author: Jérôme Brechoire
 * Email: brechoire.j@gmail.com
 */

namespace AppBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class AboutController extends Controller
{
    public function aboutAction()
    {
        return $this->render('default/apropos.html.twig');
    }
}