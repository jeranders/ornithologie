<?php

namespace AppBundle\Controller;

use ObservationBundle\Entity\Observation;
use ObservationBundle\Form\ObservationType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use UserBundle\Entity\User;
use UserBundle\Form\UserType;

class DefaultController extends Controller
{
    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request)
    {
        $form = $this->get('app.observation')->saisieObservationRapide($request);
        $countInfo = $this->get('app.observation')->countInfo();
        $imageJour = $this->get('app.observation')->imageJour();
        $obsCoo = $this->get('app.observation')->allObsMap();

        if ($form->isSubmitted() && $form->isValid()) {
            return $this->redirectToRoute('accueil');
        }

        return $this->render('default/index.html.twig', array(
            'countInfo' => $countInfo,
            'form' => $form->createView(),
            'obsCoo' => $obsCoo,
            'imageJour' => $imageJour
        ));
    }
}

