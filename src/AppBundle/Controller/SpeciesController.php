<?php
/**
 * Author: Jérôme Brechoire
 * Email: brechoire.j@gmail.com
 */

namespace AppBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class SpeciesController
 * @package AppBundle\Controller
 */
class SpeciesController extends Controller
{
    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function speciesAction(Request $request)
    {
        $listTaxref = $this->get('app.observation')->listeEspece($request->query->getInt('page', 1),8);

        return $this->render('default/liste_fiches.html.twig', array(
            'listTaxref' => $listTaxref
        ));
    }
}