<?php
/**
 * Author: Jérôme Brechoire
 * Email: brechoire.j@gmail.com
 */

namespace AppBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * Class PoilcyController
 * @package AppBundle\Controller
 */
class PoilcyController extends Controller
{
    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function policyAction()
    {
        return $this->render('default/privacy_policy.html.twig');
    }
}