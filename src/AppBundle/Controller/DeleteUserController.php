<?php
/**
 * Author: Jérôme Brechoire
 * Email: brechoire.j@gmail.com
 */

namespace AppBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * Class DeleteUserController
 * @package AppBundle\Controller
 */
class DeleteUserController extends Controller
{
    /**
     * @param $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteUserAction($id)
    {
        $this->denyAccessUnlessGranted(
            array('ROLE_PARTICULIER', 'ROLE_NATURALISTE'),
            null,
            'Vous n\'avez pas acces à cette page'
        );

        $userService = $this->get('app.user');
        $article = $userService->findOneUser(['id' => $id]);
        $userService->deleteUser($article);
        $this->addFlash('notice', 'Votre compte est bien supprimé ');

        return $this->redirectToRoute('accueil');
    }
}