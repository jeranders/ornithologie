<?php
/**
 * Author: Jérôme Brechoire
 * Email: brechoire.j@gmail.com
 */

namespace AppBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class CarnetController
 * @package AppBundle\Controller
 */
class CarnetController extends Controller
{
    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function carnetAction(Request $request)
    {
        $this->denyAccessUnlessGranted(
            array('ROLE_PARTICULIER', 'ROLE_NATURALISTE'),
            null,
            'Vous n\'avez pas acces à cette page'
        );

        $newPassword = $this->get('app.user')->ModifPassword($request);
        if ($newPassword->isSubmitted() && $newPassword->isValid())
        {
            $this->addFlash('notice','Mot de passe modifié');
            return $this->redirectToRoute('carnet');
        }

        $carnet = $this->get('app.observation')->carnetTerrain();

        return $this->render('default/carnet_terrain.html.twig', array(
            'carnet' => $carnet,
            'newPassword' => $newPassword->createView()
        ));
    }
}