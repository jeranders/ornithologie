<?php
/**
 * Author: Jérôme Brechoire
 * Email: brechoire.j@gmail.com
 */

namespace AppBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * Class CarteController
 * @package AppBundle\Controller
 */
class CarteController extends Controller
{
    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function carteAction()
    {
        $obsCoo = $this->get('app.observation')->allObsMap();
        $lastObs = $this->get('app.observation')->lastObsCarte();
        $afficheGps = $this->get('app.observation')->afficheGpsService();

        return $this->render('default/carte.html.twig', array(
            'lastObs' => $lastObs,
            'obsCoo' => $obsCoo,
            'afficheGps' => $afficheGps
        ));
    }
}