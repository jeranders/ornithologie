<?php
/**
 * Author: Jérôme Brechoire
 * Email: brechoire.j@gmail.com
 */

namespace AppBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * Class SearchController
 * @package AppBundle\Controller
 */
class SearchController extends Controller
{
    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function searchAction(Request $request)
    {
        $search = $request->get('recherche');

        $posts = $this->getDoctrine()->getRepository('ImportBundle:Taxref')
            ->searchRepository($search);

        $posts_count = $this->getDoctrine()->getRepository('ImportBundle:Taxref')
            ->searchCountRepository($search);

        return $this->render('default/resultats_recherche.html.twig', array(
            'search' => $posts,
            'search_count' => $posts_count
        ));
    }
}