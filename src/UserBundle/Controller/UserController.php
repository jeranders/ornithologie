<?php

namespace UserBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class UserController extends Controller
{

    /**
     * @param Request $request
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     * @Route("/register", name="register")
     * @Template("default/register.html.twig")
     */
    public function registerAction(Request $request)
    {
        $form = $this->get('app.user')->registerUser($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->addFlash('notice','Félicitation ' .  $username = $form["username"]->getData() . ' vous venez de vous enregistrer');
            return $this->redirectToRoute('accueil');
        }

        return array(
            'form_register' => $form->createView()
        );
    }

    /**
     * @return array
     * @Route("/login", name="login")
     * @Template("default/login.html.twig")
     */
    public function loginAction()
    {
        $helper = $this->get('security.authentication_utils');
        
        return array(
            '_username' => $helper->getLastUsername(),
            'error' => $helper->getLastAuthenticationError(),
        );
    }

    /**
     * @param Request $request
     * @Route("/set-new-password", name="set_new_password")
     * @Template("default/set_new_password.html.twig")
     */
    public function setNewPasswordController(Request $request)
    {

    }

    /**
     * @throws \Exception
     * @Route("/logout", name="logout")
     */
    public function logoutAction()
    {
        throw new \Exception('this should not be reached!');
    }

}