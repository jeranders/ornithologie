<?php

namespace UserBundle\Service;

use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use UserBundle\Entity\User;
use UserBundle\Form\Type\ModifPasswordType;
use UserBundle\Form\Type\UserType;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoder;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\RouterInterface;
use UserBundle\Repository\UserRepository;


class UserService
{
    /**
     * @var EntityManager
     */
    private $doctrine;

    /**
     * @var FormFactory
     */
    private $form;

    /**
     * @var UserPasswordEncoder
     */
    private $passEncoder;

    /**
     * @var AuthenticationUtils
     */
    private $loginUtils;

    /**
     * @var TokenStorage
     */
    private $security;

    /**
     * @var UserRepository
     */
    private $userRepo;

    /**
     * UserService constructor.
     * @param EntityManager $doctrine
     * @param FormFactory $form
     * @param UserPasswordEncoder $passEncoder
     * @param AuthenticationUtils $loginUtils
     * @param TokenStorage $security
     * @param UserRepository $userRepo
     */
    public function __construct(EntityManager $doctrine, FormFactory $form, UserPasswordEncoder $passEncoder, AuthenticationUtils $loginUtils, TokenStorage $security, UserRepository $userRepo)
    {
        $this->doctrine = $doctrine;
        $this->form = $form;
        $this->passEncoder = $passEncoder;
        $this->loginUtils = $loginUtils;
        $this->security = $security;
        $this->userRepo = $userRepo;
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\Form\FormInterface
     */
    public function registerUser(Request $request)
    {
        $user = new User();

        $form = $this->form->create(UserType::class, $user);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid())
        {
            $form->getData();

            // Si l'utilisateur est un particulier
            if ($form['typeUser']->getData() == 1)
            {
                $user->setFirstname('Aucun');
                $user->setCodeasso('Aucun');
                $user->setLastname('Aucun');
                $user->setRoles('ROLE_PARTICULIER');
            }
            else
            {
                $user->setRoles('ROLE_NATURALISTE');
            }

            $password = $this->passEncoder->encodePassword(
                $user,
                $user->getPassword()
            );

            $user->setPassword($password);

            $this->doctrine->persist($user);
            $this->doctrine->flush();
        }

        return $form;

    }

    /**
     * @return array
     */
    public function loginUser()
    {
        return array(
            'last_username' => $this->loginUtils->getLastUsername(),
            'error' => $this->loginUtils->getLastAuthenticationError(),
        );
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\Form\FormInterface
     */
    public function ModifPassword(Request $request)
    {
        $user = $this->security->getToken()->getUser();
        $form = $this->form->create(ModifPasswordType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $password = $this->passEncoder->encodePassword(
                $user,
                $user->getPlainPassword()
            );
            $user->setPassword($password);

            $this->doctrine->persist($user);
            $this->doctrine->flush();

        }
        return $form;
    }

    public function findOneUser($id)
    {
        $user = $this->userRepo->findOneBy($id);
        return $user;
    }

    /**
     * @param $user
     */
    public function deleteUser($user)
    {
        $this->doctrine->remove($user);
        $this->doctrine->flush();
        $this->security->setToken(null);
    }
}