<?php

namespace UserBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\NotNull;

class ModifPasswordType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('plainPassword', RepeatedType::class, array(
                'type' => PasswordType::class,
                'first_options'  => array('label' => 'Nouveau mot de passe'),
                'second_options' => array('label' => 'Confirmation'),
                'invalid_message' => 'Les mots de passe ne sont pas identiques',
                'required' => true,
                'constraints' => array(
                    new Length(array(
                        'min' => 6,
                        'minMessage' => 'Le mot de passe doit faire 6 caractères minimum',
                        'max' => 255,
                        'maxMessage' => 'Le mot de passe doit faire 255 caractères maximum'
                    )),
                    new NotBlank(array(
                        'message' => 'Mot de passe obligatoire'
                    )),
                    new NotNull(array(
                        'message' => 'Mot de passe obligatoire'
                    ))
                )
            ))
            ->add('oldPassword', PasswordType::class, array(
                'label' => 'Mot de passe actuel',
            ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'validation_groups' => array('register'),
            'data_class' => 'UserBundle\Entity\User'
        ));
    }

    public function getName()
    {
        return 'user_bundle_modif_password_type';
    }
}
