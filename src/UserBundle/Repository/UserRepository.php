<?php

namespace UserBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * Class UserRepository
 * @package UserBundle\Repository*
 */
class UserRepository extends EntityRepository
{
    /**
     * @return mixed
     */
    public function countUser()
    {
        return $this->createQueryBuilder('u')
            ->select('COUNT(u.id)')
            ->getQuery()
            ->getSingleScalarResult();
    }

    /**
     * @param $id
     * @return array
     */
    public function deleteUser($id)
    {
        return $this->createQueryBuilder('u')
            ->delete('u.id')
            ->where('u.id = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getResult();
    }
}