<?php

namespace UserBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\Validator\Constraints as SecurityAssert;

/**
 * @ORM\Entity(repositoryClass="UserBundle\Repository\UserRepository")
 * @ORM\Table(name="user")
 * @UniqueEntity(fields="email", message="Adresse e-mail déjà utilisé")
 * @UniqueEntity(fields="username", message="Nom d'utilisateur déjà utilisé")
 *
 */
class User implements UserInterface, \Serializable
{
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(type="string", unique=true)
     * @Assert\NotBlank()
     * @Assert\Length(
     *     min="3",
     *     max="40",
     *     minMessage = "Le nom d'utilisateur doit faire 3 caractères minimum",
     *     maxMessage = "Le nom d'utilisateur doit faire 40 caractères maximum"
     * )
     */
    private $username;

    /**
     * @var string
     *
     * @ORM\Column(type="string", unique=true)
     * @Assert\NotBlank()
     * @Assert\Email(
     *     message = "L'adresse email est invalide",
     *     checkMX = true
     * )
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     * @Assert\Length(
     *     min="6",
     *     max="255",
     *     minMessage = "Le mot de passe doit faire 6 caractères minimum",
     *     maxMessage = "Le mot de passe doit faire 255 caractères maximum"
     * )
     */
    private $password;

    /**
     * @SecurityAssert\UserPassword(
     *     groups = {"register"},
     *     message = "Mauvais mot de passe"
     * )
     */
    private $oldPassword;

    /**
     * @var String
     */
    private $plainPassword;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $typeUser;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     * @Assert\Length(
     *    min="2",
     *    max="40",
     *    minMessage = "Le Nom doit faire 2 caractères minimum",
     *    maxMessage = "Le Nom doit faire 40 caractères maximum"
     * )
     */
    private $firstname;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     * @Assert\Length(
     *    min="2",
     *    max="40",
     *    minMessage = "Le Prénom doit faire 2 caractères minimum",
     *    maxMessage = "Le Prénom doit faire 40 caractères maximum"
     * )
     */
    private $lastname;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     * @Assert\EqualTo(
     *     value = "NAO2017Naturaliste",
     *     message = "Le code asso est invalide"
     * )
     */
    private $codeasso;

    /**
     * @var array
     *
     * @ORM\Column(name="roles", type="string")
     */
    private $roles;


    /**
     * User constructor.
     */
    public function __construct() {
        $this->roles = array('ROLE_PARTICULIER', 'ROLE_NATURALISTE');
    }


    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param string $username
     */
    public function setUsername($username)
    {
        $this->username = $username;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @return string
     */
    public function getTypeUser()
    {
        return $this->typeUser;
    }

    /**
     * @param string $typeUser
     */
    public function setTypeUser($typeUser)
    {
        $this->typeUser = $typeUser;
    }

    /**
     * @return string
     */
    public function getCodeasso()
    {
        return $this->codeasso;
    }

    /**
     * @param string $codeasso
     */
    public function setCodeasso($codeasso)
    {
        $this->codeasso = $codeasso;
    }

    /**
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * @return mixed
     */
    public function getOldPassword()
    {
        return $this->oldPassword;
    }

    /**
     * @param mixed $oldPassword
     */
    public function setOldPassword($oldPassword)
    {
        $this->oldPassword = $oldPassword;
    }

    /**
     * @return mixed
     */
    public function getPlainPassword()
    {
        return $this->plainPassword;
    }

    /**
     * @param mixed $plainPassword
     */
    public function setPlainPassword($plainPassword)
    {
        $this->plainPassword = $plainPassword;
    }


    /**
     * @return string
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * @param string $firstname
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;
    }

    /**
     * @return string
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * @param string $lastname
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;
    }

    /**
     * @return array
     */
    public function getRoles()
    {
        return array($this->roles);
    }

    /**
     * @param $roles
     * @return $this
     */
    public function setRoles($roles)
    {
        $this->roles = $roles;
        return $this;
    }

    /**
     * @param $role
     * @return mixed
     */
    public function isGranted($role)
    {
        return in_array($role, $this->getRoles());
    }

    /**
     * Returns the salt that was originally used to encode the password.
     *
     * {@inheritdoc}
     */
    public function getSalt()
    {
        return null;
    }

    /**
     * Removes sensitive data from the user.
     *
     * {@inheritdoc}
     */
    public function eraseCredentials()
    {

    }

    /** @see \Serializable::serialize() */
    public function serialize()
    {
        return serialize(array(
            $this->id,
            $this->username,
            $this->password,
            // see section on salt below
            // $this->salt,
        ));
    }

    /** @see \Serializable::unserialize() */
    public function unserialize($serialized)
    {
        list (
            $this->id,
            $this->username,
            $this->password,
            // see section on salt below
            // $this->salt
            ) = unserialize($serialized);
    }
}